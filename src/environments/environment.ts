import { ApiRouter, ON, OFF } from "./env-utils";
const backend = ApiRouter("localhost:9000", "template", OFF);

export const environment = {
  production: false,
  template: {
    create: backend.route('create'),
    find: backend.route('find'),
    all: backend.route('all'),
    render: backend.route('render'),
  }
};