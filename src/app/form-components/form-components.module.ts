import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NazzInputFileComponent } from './nazz-input-file/nazz-input-file.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [
    NazzInputFileComponent, 
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  exports: [
    NazzInputFileComponent
  ]
})
export class FormComponentsModule { }
