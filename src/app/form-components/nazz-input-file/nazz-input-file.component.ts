import { AfterViewInit, Component, ElementRef, EventEmitter, Input, OnChanges, OnInit, Output, Renderer2, SimpleChanges, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-nazz-input-file',
  templateUrl: './nazz-input-file.component.html',
  styleUrls: ['./nazz-input-file.component.scss']
})
export class NazzInputFileComponent implements OnInit, OnChanges, AfterViewInit {

  public name;
  public dirty = false;
  public reset$: Subject<any> = new Subject<any>();
  public errors = {
    format: false,
    size: false
  };

  @Input() type?;
  @Input() sizeMax?;
  @Input() reset?;
  @Input() key?;
  @Output() onRead?: EventEmitter<any> = new EventEmitter<any>();
  @Output() fileSelected: EventEmitter<any> = new EventEmitter<any>();
  @ViewChild("file", {static: false}) file: ElementRef<any>;
  
  constructor(private renderer2: Renderer2) { }

  ngAfterViewInit(): void {
    this.reset$.subscribe(()=>{
      this.file.nativeElement.value = null;
    })
  }

  ngOnChanges(changes: SimpleChanges): void {
    if(changes.reset && changes.reset.currentValue == true) {
      this.reset$.next(true);
    }
  }

  ngOnInit() {
    this.setName("");
  }

  protected setName(name) {
    this.name = name;
  }

  onFileChange($event) {
    this.dirty = true;

    let files = $event.target.files || [];
    if(!files.length) return;

    let fileSelected = files[0];

    this.validateSize(fileSelected);
    this.validateType(fileSelected);

    if(this.invalid()) {
      this.setName("");
      this.fileSelected.next("");  
    }

    if(this.valid()) {
      this.setName(fileSelected.name);
      this.read(fileSelected);
      this.fileSelected.next(fileSelected);  
    }
  }

  protected read(file) {
    const reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = () => {
      let result = reader.result as string;
      this.onRead.next({stream: result, type: file.type});
    };
  }


  protected validateSize(file) {
    if(!this.sizeMax) return;
    let maxSize = file.size > this.sizeMax;
    this.errors.size = maxSize;
  }

  protected validateType(file) {
    if(!this.type) return;
    let fileType = file.name.split(".")[1];
    let isContain = this.type.filter(x=>fileType.indexOf(x)>-1).length > 0;
    this.errors.format = isContain ? false : true;
  }

  protected invalid() {
    return Object.keys(this.errors).filter(x=>this.errors[x]).length > 0;
  }

  protected valid() {
    return !this.invalid();
  }

}
