import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NazzInputFileComponent } from './nazz-input-file.component';

describe('NazzInputFileComponent', () => {
  let component: NazzInputFileComponent;
  let fixture: ComponentFixture<NazzInputFileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NazzInputFileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NazzInputFileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
