import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TemplateServices } from '../services/template.services';
import { saveAs } from 'file-saver';

@Component({
  selector: 'app-template-detail',
  templateUrl: './template-detail.component.html',
  styleUrls: ['./template-detail.component.scss']
})
export class TemplateDetailComponent implements OnInit {

  public template;
  public templateId;
  constructor(
    private templateServices: TemplateServices,
    private activateRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.setTemplateId(this.activateRoute.snapshot.params.id);
    this.setTemplate({attrs: [], contentBase64: ''});
    this.getTemplate(this.templateId);
  }

  protected setTemplateId(id) {
    this.templateId = id;
  }

  protected setTemplate(template) {
    this.template = template;
    this.template.attrs = this.template.attrs.map((attr)=>{
      attr.value = "";
      return attr;
    });
    this.template._contentParse = atob(this.template.contentBase64);
  }

  getTemplate(id) {
    this.templateServices.find({id}).subscribe({
      next: this.getTemplateOk.bind(this),
      error: this.getTemplateErr.bind(this)
    })
  }

  getTemplateOk(response) {
    console.log('getTemplateOk', response);
     this.setTemplate(response);
  }

  getTemplateErr(errr) {
    console.log('getTemplateErr', errr);
  }

  modelChanged(attr) {
    let template  =  atob(this.template.contentBase64);
    return (attrValue)=>{
      this.template._contentParse = template.replace("{{"+attr.name+"}}", attrValue);
    }
  }

  get schemaValid() {
    return this.template.attrs.filter(attr=>attr.value != "").length == this.template.attrs.length;
  }

  renderTemplate() {

    let attrs = this.template.attrs.reduce((carry, attr)=>{
      carry[attr.name] = attr.value;
      return carry;
    }, {});

    this.renderTemplateAction({
      templateId: this.templateId,
      templateParams: JSON.stringify(attrs)
    });
    
  }

  protected renderTemplateAction(command) {
    this.templateServices.render(command)
    .subscribe({
      next: this.renderTemplateOk.bind(this),
      error: this.renderTemplateErr.bind(this)
    })
  }

  protected renderTemplateOk(response) {
    console.log("renderTemplateOk");
    saveAs(response, this.template.name.concat('.pdf'));
  }

  protected renderTemplateErr(errr) {
    console.log("renderTemplateErr", errr);
  }


}
