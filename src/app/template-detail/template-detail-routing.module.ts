import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplateDetailComponent } from './template-detail.component';

const routes: Routes = [
  {
    path: "",
    component: TemplateDetailComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateDetailRoutingModule { }
