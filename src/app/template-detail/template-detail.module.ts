import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplateDetailRoutingModule } from './template-detail-routing.module';
import { TemplateDetailComponent } from './template-detail.component';
import { FormsModule } from '@angular/forms';
import { FormComponentsModule } from '../form-components/form-components.module';


@NgModule({
  declarations: [TemplateDetailComponent],
  imports: [
    CommonModule,
    TemplateDetailRoutingModule,
    FormsModule,
    FormComponentsModule
  ]
})
export class TemplateDetailModule { }
