import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplateFactoryRoutingModule } from './template-factory-routing.module';
import { TemplateFactoryComponent } from './template-factory.component';
import { FormComponentsModule } from '../form-components/form-components.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [TemplateFactoryComponent],
  imports: [
    CommonModule,
    FormsModule,
    TemplateFactoryRoutingModule,
    FormComponentsModule
  ]
})
export class TemplateFactoryModule { }
