import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TemplateServices } from '../services/template.services';

@Component({
  selector: 'app-template-factory',
  templateUrl: './template-factory.component.html',
  styleUrls: ['./template-factory.component.scss']
})
export class TemplateFactoryComponent implements OnInit {

  public nameTemplate = ""
  public contentBase64Template = "";
  public contentTemplate = "";
  public fileTemplate = "";
  public attrs = [];
  public types = [];
  constructor(
    private router: Router,
    private templateServices: TemplateServices) { }

  ngOnInit(): void {
    this.setTypes([
      {id: 1, value: 'integer'},
      {id: 2, value: 'number'},
      {id: 3, value: 'string'}
    ]);
  }

  protected setContentTemplate(contentTemplate) {
    this.contentTemplate = contentTemplate;
  }

  protected setTypes(types) {
    this.types = types;
  }

  protected setFileTemplate(fileTemplate) {
    this.fileTemplate = fileTemplate;
  }

  protected setContentBase64Template(contentBase64Template) {
    this.contentBase64Template = contentBase64Template;
  }

  onReadFile($event) {
    let contentBase64Template = this.getContentFromTemplateBase64($event.stream);
    let contentTemplate = this.getContentFromTemplate($event.stream);
    this.setContentBase64Template(contentBase64Template);
    this.setContentTemplate(contentTemplate);
  }

  fileSelected(fileSelected) {
    if(!fileSelected) {
      this.attrs = [];
      this.contentTemplate = "";
      return;
    }
    this.setFileTemplate(fileSelected);
  }

  agregarAttr() {
    let id = this.attrs.length + 1;
    this.attrs.push({
      id: id,
      type: '',
      name: ''
    });
  }


  borrarAttr(id) {
    this.attrs = this.attrs.filter((x)=>x.id != id);
  }

  protected getContentFromTemplateBase64(contentBase64: string) {
    let contentBase64ToArray = contentBase64.split('base64,');
    let htmlBase64 = contentBase64ToArray[1];
    return htmlBase64;
  }

  protected getContentFromTemplate(contentBase64: string) {
    return atob(this.getContentFromTemplateBase64(contentBase64));
  }

  modelChanged(attr) {
    return (attrName)=>{
      let attrMustache = "{{"+attrName+"}}";
      let attrsExistIntoTemplate = this.contentTemplate.indexOf(attrMustache)>-1;
      if(!attrsExistIntoTemplate) {
        attr.error = "El atributo {{"+attrName+"}} No existe en la plantilla";
        return;
      }

      attr.error = "";
    }
  }

  get schemaValid() {
    let schemaValid = this.attrs.filter(attr=>{
      let rules = [attr.error == "", attr.name != "", attr.type !=""];
      return rules.filter(rule=>rule).length === rules.length;
    });
    let schemaValidPerfectMatch =  this.attrs.length === schemaValid.length;
    let contentTemplateIsLoad = this.contentTemplate != "";
    let hasSomeOneAttr = this.attrs.length;
    let hasNameTemplate = this.nameTemplate != "";
    return schemaValidPerfectMatch && contentTemplateIsLoad && hasSomeOneAttr && hasNameTemplate;
  }

  protected request() {
    let attrsTemplate = this.attrs.map(attr=>attr.name);
    let schemaTemplate = this.createSchema(this.attrs);
    return this.obj({
      nameTemplate: this.nameTemplate,
      contentTemplate: this.contentTemplate.replace(/(\r\n|\n|\r)/gm, ""),
      contentBase64Template: this.contentBase64Template,
      //fileTemplate: this.fileTemplate,
      attrsTemplate: attrsTemplate,
      schemaTemplate: JSON.stringify(schemaTemplate)
    })
  }

  protected obj(paramsObj) {
    return Object.assign(Object.create(null), paramsObj);
  }

  protected createSchema(attrs) {
    let schemaTemplate = attrs.reduce((schema, attr)=>{
      schema.properties[attr.name] = {
        type: attr.type
      };
      return schema;
    }, this.obj({properties: {}}));

    return schemaTemplate;
  }

  createTemplate() {
    console.log("request");
    this.templateServices.create(this.request()).subscribe({
      next: this.createTemplateOk.bind(this),
      error: this.createTemplateErr.bind(this)
    });
  }

  createTemplateOk(response) {
    console.log("createTemplateOk", response);
    this.router.navigate(['/template-list']);
  }

  createTemplateErr(err) {
    console.log("createTemplateErr", err);
  }

}
