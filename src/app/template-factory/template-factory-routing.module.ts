import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TemplateFactoryComponent } from './template-factory.component';

const routes: Routes = [
  {
    path: "",
    component: TemplateFactoryComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TemplateFactoryRoutingModule { }
