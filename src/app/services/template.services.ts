import { Injectable } from "@angular/core";
import { HttpClient } from '@angular/common/http';
import { environment } from "src/environments/environment";

@Injectable({providedIn: 'root'})

export class TemplateServices {
    constructor(private http: HttpClient) {
        
    }

    create(command) {
        return this.http.post(environment.template.create.url, command);
    }

    all() {
        return this.http.get(environment.template.all.url);
    }

    find(params) {
        return this.http.get(environment.template.find.url, {params: params});
    }

    render(params) {
        return this.http.post(environment.template.render.url, params, {
            responseType: 'blob'
          });
    }
}