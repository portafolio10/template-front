import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { TemplateServices } from 'src/app/services/template.services';

@Component({
  selector: 'app-template-list',
  templateUrl: './template-list.component.html',
  styleUrls: ['./template-list.component.scss']
})
export class TemplateListComponent implements OnInit {

  public templates = [];
  constructor(private router: Router, private templateServices: TemplateServices) { }

  ngOnInit(): void {
    this.getTemplates();
  }

  protected setTemplates(templates) {
    this.templates = templates;
  }


  verDetalles(id) {
    this.router.navigate(["/template-detail/:id".replace(":id", id)]);
  }

  getTemplates() {
    this.templateServices.all().subscribe({
      next: this.templateOk.bind(this),
      error: this.templateErr.bind(this)
    })
  }

  templateOk(response) {
    console.log('templateOk', response);
     this.setTemplates(response);
  }

  templateErr(errr) {
    console.log('templateErr', errr);
  }

}
