import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TemplateListRoutingModule } from './template-list-routing.module';
import { TemplateListComponent } from './template-list.component';


@NgModule({
  declarations: [TemplateListComponent],
  imports: [
    CommonModule,
    TemplateListRoutingModule
  ]
})
export class TemplateListModule { }
